//Version 1.0
//David Rodríguez Lorenzo DTE-Universidad Rey Juan Carlos
/**This code implements functionality for these sensor:
Accelerometer - (mxc6202xJ/V)
Temperature and relative humidity - sht11
Luminosity - tsl2550
**/
/**
Frecuency samples is one mesuremente per minute. The data will be send to the SAN_Manager each hour.
While the SAN_endpoint is not active, we put the system in sleep mode.
Take the system out from sleep mode, is implemented by internal timer interruption.
**/
/*
  The data will be stored in the SD card with this structure
  SAN_ENDPOINT_ID#Temporal_flag;TEMP(ºC);HUM(%);accx(c);accy(c);lum(lumens)#
  

*/


#include <Arduino.h>
#include <Wire.h> //Library for communication with accelerometer and luminosity sensor.It work at 100Khz by default.
#include <SD.h> //microSD library

//libraries for interrrupts and power management
#include <avr/io.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

//SAN_EndPoint_ID
char SAN_ENDPOINT_ID[100];
char timeOrigin[100];

//ZigBee pin connection
int sleepZigbeePin=10;

//Pin connection and variable declaration for SD card.
File dataFile;
const int chipSelect = 4; 
unsigned long offset=0;
//Variable declarartion for luminosity sensor
int Luminosity_addr (0x39);//Internal address
byte valueChannel0byte=0;
byte valueChannel1byte=0;
float lightLevel=0;

//Variable declaration for acelerometer
int accX, accY=0;
int acelerometer_Addr=0x10;//internal address for accelerometer.
                          /*esta es la direccion que aparece en el datasheet, esto es un poco ambiguo por que
                          segun la tabla sería 20h = 32, pero luego en el ejemplo de transmision es 16 o 0x10(Esta es la válida)*/

//Declaration variables and pin connection for SHT11 temperarture and relative humidity
int shtData=8;
int shtCLK=7;
double temp, hum=0;

//Variables declaration for timing
char tFlag;
int fSample=16;
int tSend=24;
volatile int cont,cont2=0; 




void setup(){
  Wire.begin();
  Serial.begin(9600);
  Serial.print("Initializting SD card...");//It prepare a SD.
  pinMode(chipSelect, OUTPUT);
  while (!SD.begin(chipSelect)) {
    Serial.println("Initialization failure");
    delay(100);
    //return;
  }
  Serial.println("Initialisation complete.");
  if(SD.remove("data.txt")){
      Serial.println("SD Erased");
  }
  
  setSettings();
  
}
//This codes will be execute when the interrupt is active
ISR(WDT_vect){
  cont=cont++;  //Sample frecuency  timer
  cont2=cont2++;// Send data frecuency timer
}


void loop(){
 
  tempManage();
 
}
/*--------------------------------------------------------------------------------------------------------------
  Set somo configuration settings
-----------------------------------------------------------------------------------------------------------------*/
void setSettings(){
  delay(50); //this time let the sensor be ready
  //Configures luminosity sensor to standar mode
  configSenLumi(0x18);
  //In future revision we can send from server some information to endpoints
  waitForConfig();
  //Pu zigbee module to sleep mode
  zigBeeSleep();
  //Configure whatchDog
  watchDogOn();


}
/*--------------------------------------------------------------------------------------------------------------
  Wait for some configuration parameters
-----------------------------------------------------------------------------------------------------------------*/
void waitForConfig()
{
  char command_in[200];
  int comm_ind = 0;
  boolean comm_complete = false;
  char c;
  while(!comm_complete){
    while (Serial.available()) {
      
      c = (char)Serial.read(); 
      Serial.print(c);
      command_in[comm_ind] = c;
      comm_ind += 1;
      if (c == 'x') {
        comm_complete = true;
       
      } 
    }
  }
  
    if(comm_complete){
      
      char* comm_split[50];
      split(command_in, comm_split, "&");
      sprintf(SAN_ENDPOINT_ID,"%s",comm_split[0]); 
      sprintf(timeOrigin,"%s",comm_split[1]);
      
    }
      

}



/*--------------------------------------------------------------------------------------------------------------
  Manage functionality of the endPoint
-----------------------------------------------------------------------------------------------------------------*/

void tempManage(){

  while(cont<(fSample/8)){    //This implements sample frecuency, its value is a 8 integer multiple
    sleepModeON();//The program will continue from here after the WDT timeout.
                  //if cont<(fSample/8)=FALSE we enter again in sleep mode.
  }
  cont=0;
  sleepModeOFF();
  readTHRel();
  readAcc();
  readLum();
  currentTime();
  //Store data in SD card
  saveData();
  if (!(cont2<(tSend/8))){    //This implements send frecuency, its value is a 8 integer multiple
    sendData();
    cont2=0;
  }
 
  
}
/*-------------------------------------------------------------------------------------------------------------------
  Read current time from Real time clock module

---------------------------------------------------------------------------------------------------------------------*/
void currentTime(){

  tFlag='d';

}
/*----------------------------------------------------------------------------------------------------------------
Put Arduino in sleep Mode
-----------------------------------------------------------------------------------------------------------------*/
void sleepModeON(){

  //Configures PRR register , desactivate clocks, TWI,SPI,ADC,UART,TIMER0 and timer2
  PRR=(1<<7)|(1<<6)|(1<<5)|(1<<3)|(1<<2)|(1<<1)|(1<<0); 
  
  //desactivates BOD
  MCUCR|=(1<<BODS);
  MCUCR|=(1<<BODSE);
  //adc switch off
  ADCSRA=0;
  //Switch off analog comp
  ACSR|=(1<<ACD);
  //Switch off UART
  //UCSR0B|=(0<<TXEN0);
  //UCSR0B|=(0<<RXEN0);
  
  //Configures a Sleep mode, selects power down
  SMCR|=(0<<SM2);
  SMCR|=(1<<SM1);
  SMCR|=(0<<SM0);
  SMCR|=(1<<SE);
  
  sleep_mode();//Enter sleep mode
  
}
/*----------------------------------------------------------------------------------------------------------------
Wake Up arduino
-----------------------------------------------------------------------------------------------------------------*/
void sleepModeOFF(){
  sleep_disable();
  SMCR|=(0<<SE);
  power_all_enable();//Re-enable the peripherals.
}


/*----------------------------------------------------------------------------------------------------------------
Save data in SD card
-----------------------------------------------------------------------------------------------------------------*/
void saveData(){
  dataFile=SD.open("data.txt", FILE_WRITE);
  offset=dataFile.position();
  if (dataFile){
    dataFile.print("#");
    dataFile.print(SAN_ENDPOINT_ID);
    dataFile.print(";");
    dataFile.print(tFlag);
    dataFile.print(";");
    dataFile.print(temp,DEC);
    dataFile.print("ºC;");
    dataFile.print(hum,DEC);
    dataFile.print("%;");
    dataFile.print(accX,DEC);
    dataFile.print("x;");
    dataFile.print(accY,DEC);
    dataFile.print("y;");
    dataFile.print(lightLevel,DEC);
    dataFile.println("l#");
    dataFile.close();
    
   }else{
     Serial.println("Open operation ERROR in: data.txt");
   }


}

/*----------------------------------------------------------------------------------------------------------------
Send data from SD card to SAN_manager trough ZigBee
-----------------------------------------------------------------------------------------------------------------*/
void sendData(){
  
  Serial.println("Sending data from SD");
  wakeUpZigBee();
  
  dataFile=SD.open("data.txt", FILE_READ);
  if (dataFile) {
    dataFile.seek(offset);//Only send that not send yet.
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    /*while((waitACK<10)&&(!ACK)){
      //Serial.println("Waiting ACK");
      while(Serial.available()){
        c = (char)Serial.read(); 
        command_in[comm_ind] = c;
        comm_ind += 1;
        if(c=='?'){
          ACK=true;
          offset=dataFile.position();
          //command_in[comm_ind+1] = '\0';
          //split(command_in, comm_split, "&");
          //if(!strcmp(command_in, "ACK")){
            //ACK=true;
          //}  
        }
      }
      waitACK++;
      delay(1000);
    }*/
    dataFile.close();
  }
  else{
    Serial.println("Open ERROR in: data.txt");
  }
   zigBeeSleep();
  
  
  


}

/*----------------------------------------------------------------------------------------------------------------
Switch off/on ZigBee antennas
-----------------------------------------------------------------------------------------------------------------*/
void zigBeeSleep()
{
  pinMode(sleepZigbeePin,OUTPUT);
  digitalWrite(sleepZigbeePin,HIGH);
}
void wakeUpZigBee()
{
  pinMode(sleepZigbeePin,OUTPUT);
  digitalWrite(sleepZigbeePin,LOW);
  delay(10);
}

/*----------------------------------------------------------------------------------------------------------------
Configures watchDog timer
-----------------------------------------------------------------------------------------------------------------*/
void watchDogOn()
{
  
  MCUSR = MCUSR & B11110111; // Clean watchDog flag.
  WDTCSR = WDTCSR | B00011000; //
  WDTCSR = B00100001; //COnfigures watchDog to a second
  
  
  WDTCSR = WDTCSR | B01000000; //active watchDog interrupts
  MCUSR = MCUSR & B11110111; // Reset flag state
}
  
  
  
/*----------------------------------------------------------------------------------------------------------------
Relative humidity and Temperature measurement
-----------------------------------------------------------------------------------------------------------------*/ 
  
void readTHRel(){
  int ack =0;
  byte command;
  int tempData=0;
  int RHdata=0;
  float tempDataCor=0;
  double RHdataCor=0;
  double RHdataCor2=0;
  //Paramenters for Version 3 of SHT11
  /*double c1=-2.0468;
  double c2=0.0367;
  double c3 =-0.0000015955;
  double t1=0.01;
  double t2=0.00008;*/
  //Paramenters for Version 4 of SHT11
  double c1=-4;
  double c2=0.0405;
  double c3 =-0.0000028;
  double t1=0.01;
  double t2=0.00008;
  float d1=-40.1;    //It is different for each in voltage value
  float d2=0.01;

  tempData=0;
  RHdata=0;
  RHdataCor=0;
  RHdataCor2=0;
  tempDataCor=0;
  
  /** --------------------------------------------------------------------------------------------------------
  Temperature Measurement
  --------------------------------------------------------------------------------------------------------*/
  //Begin a comunication, send a START protocol to sensor
  pinMode(shtData, OUTPUT);
  pinMode(shtCLK,OUTPUT);
  digitalWrite(shtData, HIGH);
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, LOW);
  delay(5);
  digitalWrite(shtCLK, LOW);
  delay(2);
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, HIGH);
  delay(5);
  digitalWrite(shtCLK,LOW);
  delay(5);
  //00000011=0x03 14 bits)
  command=0x03;
  ack=0;
  byte value;
  for (int i=7; i>=0; i--){
    value=bitRead(command,i);
    if (value==1){
      digitalWrite(shtData,HIGH);
    }
    else{
      digitalWrite(shtData,LOW);
    }
    digitalWrite(shtCLK,HIGH);
    delay(10);
    digitalWrite(shtCLK,LOW);
    delay(10);
  }
  //Receive an ACK from sensor
  digitalWrite(shtCLK,HIGH);
  pinMode(shtData, INPUT);
  delay(10);
  ack=digitalRead(shtData);
  if(ack!=LOW){
    //ack=digitalRead(shtData);
    Serial.println("ACK error in Temperature Measurement");
  }
  digitalWrite(shtCLK,LOW);
  
  ack=digitalRead(shtData);
  if (ack!=HIGH){
    //ack=digitalRead(shtData);
    Serial.println("ACK ERROR at finish comunication");
  }
  
  //We wait until the sensor compleete a measurement, when it happens sensor pull down the line.
  while(ack==HIGH){
    ack=digitalRead(shtData);
  }
  //take a temperature value
  int aux=0;
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    if(command==0x03){
    tempData=tempData*2+digitalRead(shtData);
    }
    if(command==0x05){
      RHdata=RHdata*2+digitalRead(shtData);
    }
    digitalWrite(shtCLK,LOW);
  }
  
  if(command==0x03){
    tempData *=256;
    }
 
  if(command==0x05){
    RHdata*=256;
  }
  //Agree first byte receive
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);
  digitalWrite(shtData,LOW);
  digitalWrite(shtCLK,HIGH);
  digitalWrite(shtCLK,LOW);
  pinMode(shtData,INPUT);
  
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    aux=aux*2+digitalRead(shtData);
    digitalWrite(shtCLK,LOW);
  }
  if(command==0x03){
    tempData=tempData+aux;
     
    
    }
  if(command==0x05){
    RHdata=RHdata+aux;
   
    
  }
 //CRC discard
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);//CRC discard
  digitalWrite(shtCLK,HIGH);
  delay(10);
  digitalWrite(shtCLK,LOW);
  delay(10);
  delay(50);
  
  
  /** --------------------------------------------------------------------------------------------------------
   Relative Humidity
  --------------------------------------------------------------------------------------------------------*/
   ////Begin a comunication, send a START protocol to sensor
  pinMode(shtData, OUTPUT);
  pinMode(shtCLK,OUTPUT);
  digitalWrite(shtData, HIGH);
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, LOW);
  delay(5);
  digitalWrite(shtCLK, LOW);
  delay(2);
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, HIGH);
  delay(5);
  digitalWrite(shtCLK,LOW);
  delay(5);
  //RHumedity resolution 14 bits
  command=0x05;
  ack=0;
  value=0;
  for (int i=7; i>=0; i--){
    value=bitRead(command,i);
    if (value==1){
      digitalWrite(shtData,HIGH);
    }
    else{
      digitalWrite(shtData,LOW);
    }
    digitalWrite(shtCLK,HIGH);
    delay(10);
    digitalWrite(shtCLK,LOW);
    delay(10);
  }
  //Receive an ACK
  digitalWrite(shtCLK,HIGH);
  pinMode(shtData, INPUT);
  delay(10);
  ack=digitalRead(shtData);
  if(ack!=LOW){
    //ack=digitalRead(shtData);
    Serial.println("ERROR RH measurement");
  }
  digitalWrite(shtCLK,LOW);
  
  ack=digitalRead(shtData);
  if (ack!=HIGH){
    //ack=digitalRead(shtData);
    Serial.println("ACK ERROR at finish comunication");
  }
  
  while(ack==HIGH){
    ack=digitalRead(shtData);
  }
  
  aux=0;
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    if(command==0x03){
    tempData=tempData*2+digitalRead(shtData);
    }
    if(command==0x05){
      RHdata=RHdata*2+digitalRead(shtData);
    }
    digitalWrite(shtCLK,LOW);
  }
  
  if(command==0x03){
    tempData *=256;
    }
  
  if(command==0x05){
    RHdata*=256;
  }
  
  
  
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);
  digitalWrite(shtData,LOW);
  digitalWrite(shtCLK,HIGH);
  digitalWrite(shtCLK,LOW);
  pinMode(shtData,INPUT);
  
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    aux=aux*2+digitalRead(shtData);
    digitalWrite(shtCLK,LOW);
  }
  if(command==0x03){
    tempData=tempData+aux;
     
    
    }
  if(command==0x05){
    RHdata=RHdata+aux;
   
    
  }
 
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);
  digitalWrite(shtCLK,HIGH);
  delay(10);
  digitalWrite(shtCLK,LOW);
  delay(10);
  delay(50);
  //Calculete tempearature anf relative humedity with calibration values
  tempDataCor=d1+d2*tempData; 
  Serial.print(tempDataCor,4);
  Serial.print("C : "); 
  RHdataCor=c1+c2*RHdata+c3*pow(RHdata,2); //Habria que usar la correción segun la temperatura
  RHdataCor2=(tempDataCor-25)*(t1+t2*RHdata)+RHdataCor;
  Serial.print(RHdataCor2,4);  
  Serial.println("%");
  temp=tempDataCor;
  hum=RHdataCor2;
  
   
  
}


/*------------------------------------------------------
Luminosity Measurement
-------------------------------------------------------*/  

void readLum(){

  //Code for ambient Luminosity measurement, for TSL2550 module.
  //The sensor will work in its standar mode.
  lumDataRead();
  lumSensorOff();
  luminosityCalc();
  Serial.println(lightLevel); 
}


void configSenLumi(int command){
  int est=0;
  //Wake up sensor
  Wire.beginTransmission(Luminosity_addr);//Luminosity sensor address
  Wire.write(0x03);
  est=Wire.endTransmission();
  //Standar mode-400ms each cahnnel
  Wire.beginTransmission(Luminosity_addr);
  Wire.write(command);
  est=Wire.endTransmission();
}
void lumDataRead(){
  int est=0;
  boolean valid=false;
  //wake up
  Wire.beginTransmission(Luminosity_addr);
  Wire.write(0x03);
  est=Wire.endTransmission();
  //Configure for perform a lecture from channel 0
  Wire.beginTransmission(Luminosity_addr);
  Wire.write(0x43);
  est=Wire.endTransmission();
  //while
  valid=false;
  while(!valid){//this can be a loop
    Wire.requestFrom(Luminosity_addr,1);
    while(Wire.available()==0){
      Serial.println("No data from channel 0");
    }
    valueChannel0byte=Wire.read();
    if (bitRead(valueChannel0byte,7)==1){ 
      valid=true;
    }
  }
  
  //Configure for perform a lecture from channel 0
  Wire.beginTransmission(Luminosity_addr);
  Wire.write(0x83);
  Wire.endTransmission();
  
  
  valid=false; 
  
  while(!valid){
    Wire.requestFrom(Luminosity_addr,1);
    while(Wire.available()==0){
      Serial.println("No data from channel 1"); 
    }
    
    valueChannel1byte=Wire.read();
    if (bitRead(valueChannel1byte,7)==1){
      valid=true;
    }
  }
}
void lumSensorOff(){
  //put sensor in sleep mode
  Wire.beginTransmission(Luminosity_addr);
  Wire.write(0x00);
  Wire.endTransmission();
}

void luminosityCalc(){
  //Method to obtain real luminosity value.
  int chordChannel0=0;
  int chordChannel1=0;
  int stepChannel0=0;
  int stepChannel1=0;
  int adcCountChannel0=0;
  int adcCountChannel1=0;
  float R= 0;
  
  chordChannel0=(valueChannel0byte-128)/16;
  chordChannel1=(valueChannel1byte-128)/16;
  stepChannel0=(valueChannel0byte-128)-chordChannel0*16; 
  stepChannel1=(valueChannel1byte-128)-chordChannel1*16;

  adcCountChannel0=int(16.5*(pow(2,chordChannel0)-1))+(stepChannel0*pow(2,chordChannel0));
  adcCountChannel1=int(16.5*(pow(2,chordChannel1)-1))+(stepChannel1*pow(2,chordChannel1));
  R=(float)adcCountChannel1/(adcCountChannel0-adcCountChannel1);
  lightLevel=(adcCountChannel0-adcCountChannel1)*0.39*exp(-0.181*pow(R,2)) ;   
  
   

}

/*------------------------------------------------------
Aceleration Measurement
-------------------------------------------------------*/ 
void readAcc(){
  AccWakeUp();
  accRead();
  Serial.print("x: ");
  Serial.print(accX);
  Serial.print(" , ");
  Serial.print("y: ");
  Serial.println(accY);
  
}
void AccWakeUp() {
  int est=0; 
  Wire.beginTransmission(acelerometer_Addr); 
  Wire.write(0x00); 
  Wire.write(0x00);
  est=Wire.endTransmission(true);
  delay(100);//Wait to let accelerometer to wake UP
}
void accRead(){
  int est=0; 
  byte x_val_l, x_val_h, y_val_l, y_val_h;
  
  Wire.beginTransmission(acelerometer_Addr);
 
  Wire.write(0x01);
  Wire.endTransmission(true);
  Wire.requestFrom(acelerometer_Addr, 4);
  while(Wire.available()==0) {
    Serial.println("No data available");
  }
 // MSB axis X
 
  x_val_h = Wire.read();
 
 // LSB axis x
  x_val_l = Wire.read();
  accX=x_val_l+x_val_h*256;
 
 // MSB axis Y 
 y_val_h = Wire.read();
 
 // LSB axis y 
 y_val_l = Wire.read();
 
 
 accY=y_val_l+y_val_h*256;
   
 //Accelerometer switch off
 Wire.beginTransmission(acelerometer_Addr);
 Wire.write(0x01);
 Wire.endTransmission();
}

/*--------------------------------------------------------------------------------------------------------------------------------------------
  Split a given string
--------------------------------------------------------------------------------------------------------------------------------------------*/
int split(char* in, char* out[], char* del){
	
	char** split_aux = out; 
        char* p;
        
	split_aux[0] = strtok_r(in, del, &p);
	int i = 0;
	while(split_aux[i] != NULL){
		i++;
		split_aux[i] = strtok_r(NULL, del, &p);
		
	}
	return i;
}




