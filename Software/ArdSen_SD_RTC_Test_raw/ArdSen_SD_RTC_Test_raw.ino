/*
 uSD card and RTC DS3234 test on ArduSense. It writes date and time in a file.
 
 This example shows how use the utility libraries on which the
 uSD library is based in order to get info about the SD card.
 Useful for testing that the uSD card works.
 	
 The uSd card circuit:
 * SD card attached to SPI bus as follows:
 ** CS - pin 10 on ArduSense as in Pro or Pro Mini (PB2, uC pin 14)
 ** MOSI - pin 11 on ArduSense as in Pro or Pro Mini (PB3, uC pin 15)
 ** MISO - pin 12 on ArduSense as in Pro or Pro Mini (PB4, uC pin 16)
 ** CLK - pin 13 on ArduSense as in Pro or Pro Mini (PB5, uC pin 17)
 
 Created 28 Mar 2011
 by Limor Fried 
 Modified 9 Apr 2012
 by Tom Igoe
 
 The RTC circuit:
 * RTC attached to SPI bus as follows:
 ** CS - pin 6 on ArduSense as in Pro or Pro Mini (PD6, uC pin 10)
 ** MOSI - pin 11 on ArduSense as in Pro or Pro Mini (PB3, uC pin 15)
 ** MISO - pin 12 on ArduSense as in Pro or Pro Mini (PB4, uC pin 16)
 ** CLK - pin 13 on ArduSense as in Pro or Pro Mini (PB5, uC pin 17)
 
 Based on DeadOn RTC - DS3234 Breakout example Sketch
 Available from: https://www.sparkfun.com/products/10160
 
 Modified 10 March 2014 for ArduSense
 by Joaquín Vaquero
 */

// Include the SD library:
#include <SD.h>
// Include the SPI library for RTC 
#include <SPI.h>

// Set up variables using the SD utility library functions:
// Construct an instance of Sd2Card, SdVolume and SdFile
Sd2Card card;
SdVolume volume;
SdFile root;

// uSD CS signal. ArduSense: pin 10
const int SD_chipSelect = 10;
// SPI Master SCK signal at pin 13 (PB5, pin 17 of ATmega328P)
// Output from the uController. uSD card as a Slave
File dataFile;
unsigned long offset = 0;

// RTC CS signal. ArduSense: pin 6
const int RTC_chipSelect = 6;  
// SPI Master SCK signal at pin 13 (PB5, pin 17 of ATmega328P)
// Output from the uController. RTC as a Slave
// Led attached to this pin
const int SCK_led = 13;
// RTC  PD2/INT* signal. ArduSense: pin 2
// PD2, uC pin 32
const int RTC_INT = 2;

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  pinMode(SCK_led,OUTPUT);
  // ¿¿¿¿SPI mode 0 for uSD????. Check if used with another SPI device
  // such as a RTC that may use a different SPI mode.
  // To use this function, include SPI.h library.
  //SPI.setDataMode(SPI_MODE0); 
  // uSD CS set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // must be left as an output or the SD library functions will not work. 
  pinMode(SD_chipSelect, OUTPUT);     // change this to 53 on a Mega
  // RTC CS set as an output by default.
  pinMode(RTC_chipSelect, OUTPUT);     
  digitalWrite(RTC_chipSelect, HIGH);   // Disables RTC to avoid SPI bus collisions
  // see if the card is present and can be initialized:
  if (!SD.begin(SD_chipSelect)) {
    Serial.println("Card failed, or not present");
    return;
  }
    Serial.print("\nInitializing SD card...");
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, SD_chipSelect)) {
    Serial.println(" initialization failed. Things to check:");
    Serial.println("* is a card is inserted?");
    Serial.println("* Is your wiring correct?");
    Serial.println("* did you change the SD_chipSelect pin to match your shield or module?");
    delay(2000);
    return;
  } 
  else {
    Serial.println("Wiring is correct and a card is present."); 
  }

  // print the type of card
  Serial.print("\nCard type: ");
  switch(card.type()) {
  case SD_CARD_TYPE_SD1:
    Serial.println("SD1");
    break;
  case SD_CARD_TYPE_SD2:
    Serial.println("SD2");
    break;
  case SD_CARD_TYPE_SDHC:
    Serial.println("SDHC");
    break;
  default:
    Serial.println("Unknown");
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
    delay(2000);
    return;
  }

  // print the type and size of the first FAT-type volume
  uint32_t volumesize;
  Serial.print("\nVolume type is FAT");
  Serial.println(volume.fatType(), DEC);
  Serial.println();

  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
  volumesize *= 512;                         // SD card blocks are always 512 bytes
  Serial.print("Volume size (bytes): ");
  Serial.println(volumesize);
  Serial.print("Volume size (Kbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  Serial.print("Volume size (Mbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  
  if(SD.exists("data.txt")){          // Remove de test file "data.txt" if exists.
    SD.remove("data.txt");
    Serial.println("File data.txt erased.");
   }
   
   RTC_init();  //Initialites RTC DS3234
  // RTC DS3234 time setup. 
  // Day(1-31), month(1-12), year(0-99), hour(0-23), minute(0-59), second(0-59)
  // Day of the week not used.
  SetTimeDate(6,2,14,13,15,50); 
}

void loop(void) {

  Serial.println("\nFiles found on the card (name, date and size in bytes): ");
  SPI.setDataMode(SPI_MODE0); 
  digitalWrite(SD_chipSelect, LOW); // Selects uSD
  root.openRoot(volume);

  // list all files in the card with date and size
  root.ls(LS_R | LS_DATE | LS_SIZE);
  saveData();
  sendData();
  Serial.println(ReadTimeDate());
  delay(2000);
}

// ======== RTC DS 32342 initialization function =====================================
int RTC_init(){ 
  // RTC CS set as an output by default.
  pinMode(RTC_chipSelect,OUTPUT);
  // PD2/INT* uC pin set as an input
  // this signal can be a square wave or an INT* signal
  // from RTC, depending on the RTC Control Register
  // configuration.
 
 // pinMode(RTC_INT,INPUT);

  // Start the SPI library:
  SPI.begin();
  // Sets the MSB order for SPI
  SPI.setBitOrder(MSBFIRST); 
  SPI.setDataMode(SPI_MODE1); // Both modes 1 & 3 should work with RTC DS3234. 
                              // Reported problems on Forums with MODE1. We didn`t
                              // have any.
  
  SPI.setClockDivider(SPI_CLOCK_DIV2); // Sets the SPI clock divider relative to the system clock. 
  // Avilable dividers are 2, 4, 8, 16, 32, 64 or 128. 
  // The default setting is SPI_CLOCK_DIV4 sets the SPI clock to one-quarter the frequency 
  // of the system clock (4 Mhz for the boards at 16 MHz).
  // ArduSense has 8 MHz clock, and RTC supports up 4 MHz
  // Changed to SPI_CLOCK_DIV2 to get RTC SPI full speed.
 
  digitalWrite(RTC_chipSelect, LOW);  // Selects RTC
  // Set Control Register 
  SPI.transfer(0x8E); // Control Register Address
  // Bit 7:EOSC*=1 Disables Oscillator when battery powered; 
  // Bit 6:BBSQW=1 Enables Battery SQ wave output on INT*/SQW pin;
  // Bit 5:CONV=0 Disable Temperature compensation; 
  // Bit 4-3:RS2-RS1=0-0 Output square wave on INT*/SQW pin at 1Hz; 
  // Bit 2:INTCN=0 Interrupt disabled; 
  // Bit 1-0:A2IE-A1IE=0-0 Alarmans Disabled; 
  // Control Register content=0x60 = b0110 0000
  SPI.transfer(0xA0);
 // Control Register content=0xA0 = b1010 0000
//  SPI.transfer(0xA0);
  digitalWrite(RTC_chipSelect, HIGH); // Deselects RTC
  delay(10);
}

// ========= RTC DS 32342 Time setup function =========================================
// Day(1-31), month(1-12), year(0-99), hour(0-23), minute(0-59), second(0-59)
int SetTimeDate(int d, int mo, int y, int h, int mi, int s){ 
  int TimeDate [7]={
    s,mi,h,0,d,mo,y  }; // Day of the week not used, set to 0
  SPI.setDataMode(SPI_MODE1); // Both modes 1 & 3 should work with RTC DS3234. 
                              // Reported problems on Forums with MODE1. We didn`t
                              // have any.
  digitalWrite(RTC_chipSelect, LOW); // Selects RTC
  for(int i=0; i<=6;i++){
    if(i==3) // Day of the week not used, skipped
      i++;
    int b= TimeDate[i]/10;   // b keeps decens.
    int a= TimeDate[i]-b*10; // a keeps units.
    if(i==2){        // Set de 24/12 Hours mode
      if (b==2)      // Set 24 hour mode
        b=B00000010;   
      else if (b==1) // Set 12 hour mode
        b=B00000001;
    }	
    TimeDate[i]= a+(b<<4);  // Adds units and decens
    SPI.transfer(i+0x80); // Timekeeping Registers Address, write mode
    SPI.transfer(TimeDate[i]); // Timekeeping Registers content, write value 
  }
   digitalWrite(RTC_chipSelect, HIGH);  // Deselects RTC
 }

// ========= RTC DS 32342 Time Read function =========================================
String ReadTimeDate(){
  String temp;
  int TimeDate [7]; // Second,minute,hour,null,day,month,year
                    // Day of the week not used, set to 0		
  SPI.setDataMode(SPI_MODE1); // Both modes 1 & 3 should work with RTC DS3234. 
                              // Reported problems on Forums with MODE1. We didn`t
                              // have any.
  digitalWrite(RTC_chipSelect, LOW); // Selects RTC
  for(int i=0; i<=6;i++){
//    if(i==3)  // Day of the week not used, skipped
//      i++;
//    digitalWrite(RTC_chipSelect, LOW); // Selects RTC
    SPI.transfer(i+0x00); // Timekeeping Registers Address, read mode
    unsigned int n = SPI.transfer(0x00); // Timekeeping Registers content, read value        
    if (n<10) {      // Includes 0 placeholders for single digit values
      temp.concat("0"); }  
    temp.concat(n); // Timekeeping Registers content, 
                    // read value and adds it to a string 
    Serial.println(n,DEC);
                                                   
  }
    digitalWrite(RTC_chipSelect, HIGH); // Deselects RTC
    return(temp);
//    int a=n & B00001111;    // a keeps units.
//    if(i==2){	
//      int b=(n & B00110000)>>4; // Checks bit 5 of the hour register 0x02
//      if(b==B00000010)          // If reads 1, 24 hour mode
//        b=20;        
//      else if(b==B00000001)     // If reads 0, 12 hour mode
//        b=10;
//      TimeDate[i]=a+b;          // Sets hour data according to the hour mode
//    }
//    else if(i==4){              // Sets decens for Days
//      int b=(n & B00110000)>>4;
//      TimeDate[i]=a+b*10;
//    }
//    else if(i==5){              // Sets decens for Months
//      int b=(n & B00010000)>>4;
//      TimeDate[i]=a+b*10;
//    }
//    else if(i==6){              // Sets decens for Years
//      int b=(n & B11110000)>>4;
//      TimeDate[i]=a+b*10;
//    }
//    else{	                // Sets decens for Minutes and Seconds
//      int b=(n & B01110000)>>4;
//      TimeDate[i]=a+b*10;	
//    }
// }
//  
//  //Set time string format as dd/mm/yyyy  hh:mm:ss
//  
//  if (TimeDate[4]<10) {      // Includes 0 placeholders for single digit values
//      temp.concat("0"); }  
//  temp.concat(TimeDate[4]);
//  temp.concat("/") ;
//  if (TimeDate[5]<10) {      // Includes 0 placeholders for single digit values
//      temp.concat("0"); }  
//  temp.concat(TimeDate[5]);
//  temp.concat("/") ;
//  temp.concat("20");
//  temp.concat(TimeDate[6]);
//  temp.concat("     ") ;
// if (TimeDate[2]<10) {      // Includes 0 placeholders for single digit values
//      temp.concat("0"); }  
//  temp.concat(TimeDate[2]);
//  temp.concat(":") ;
// if (TimeDate[1]<10) {      // Includes 0 placeholders for single digit values
//      temp.concat("0"); }  
//  temp.concat(TimeDate[1]);
//  temp.concat(":") ;
// if (TimeDate[0]<10) {      // Includes 0 placeholders for single digit values
//      temp.concat("0"); }  
//  temp.concat(TimeDate[0]);
//  return(temp);
}

// ======== Save data in SD card =========================================

void saveData(){
  Serial.println("Writting data into SD");
  SPI.setDataMode(SPI_MODE0); 
  digitalWrite(SD_chipSelect, LOW); // Selects uSD
  dataFile = SD.open("data.txt", FILE_WRITE); // Opens file "data.txt", write mode
  offset = dataFile.position();
  if (dataFile){
    dataFile.seek(offset);        // Writes in the next Only not previously sent data
    dataFile.print("Offset: ");
    dataFile.print(offset, DEC);
    dataFile.println(",eol");
    dataFile.close();            // Closes file "data.txt"
    }
  else{
     Serial.println("Open ERROR (write mode) in: data.txt");
  }
  digitalWrite(SD_chipSelect, HIGH); // Deselects uSD 
}

// ======== Send data from SD card to SAN_manager trough ZigBee  =========

void sendData(){
  Serial.println("Sending data from SD");
  SPI.setDataMode(SPI_MODE0); 
  digitalWrite(SD_chipSelect, LOW);            // Selects uSD
  dataFile = SD.open("data.txt", FILE_READ);   // Opens file "data.txt", read mode
  if (dataFile) {
    dataFile.seek(offset);                   // Only not previously sent data
    while (dataFile.available()) {
    Serial.write(dataFile.read());
    }
    dataFile.close();                      // Closes file "data.txt"    
    Serial.print("Numero : ");
    Serial.println(offset,DEC);
  }
  else{
    Serial.println("Open ERROR (read mode) in: data.txt");
  }
  digitalWrite(SD_chipSelect, HIGH); // Deselects uSD 
}
