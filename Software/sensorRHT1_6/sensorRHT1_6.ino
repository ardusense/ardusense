//Version 1.6
//David Rodríguez Lorenzo Universidad Rey Juan Carlos
/**El código sensorRHT1_6, muestra los valores de temperatura y humedad en su valor decimal, 
habiendo aplicado las fórmulas del datasheet, incluyendo correcion por temperatura
para obtener los valores.  Todos estos calculos son para el modo estandar y una tensión de alimentación de 5V.
NOTA: faltan por aplicar formulas de DEW-POINT.
Muestreo de las dos variables cada 4 segundos para no producir autocalentamiento
Se ha intentado optimizar el codigo
*/
 
#include <Arduino.h>
#include <SD.h> //para la parte de la microSD
 
// Variables y conexiones para el sensor

int shtData=8; //PB0, pin 12 para ATmega328
int shtCLK=7;  //PD7, pin 11 para ATmega328
int ack =0;
byte comando;
int datosTemp=0;
int datosHumedad=0;
float datosTempCorregido=0;
double datosHumedadCorregido=0;
double datosHumedadCorregido2=0;//correcion por temperatura para 12 bits
//parametro para la version 3 del sensor
/*double c1=-2.0468;
double c2=0.0367;
double c3 =-0.0000015955;
double t1=0.01;
double t2=0.00008;*/
//parametro para la version 4 del sensor
double c1=-4;
double c2=0.0405;
double c3 =-0.0000028;
double t1=0.01;
double t2=0.00008;
float d1=-40.1;    //depende de la tension de entrada d1=-39.7;
float d2=0.01;


void setup()
{
   Serial.begin(9600); // Open serial connection to report values to host
   Serial.println("Starting up");
   delay(11); //para que arranque el sensor y se ponga en sleep mode.
               //no se puede hacer nada durante este tiempo.
}
 
void loop()
{
 datosTemp=0;
 datosHumedad=0;
 datosHumedadCorregido=0;
 datosHumedadCorregido2=0;//correcion por temperatura para 12 bits
 datosTempCorregido=0;
 /** --------------------------------------------------------------------------------------------------------
  OBTENER TEMPERATURA
  --------------------------------------------------------------------------------------------------------*/
  //empezamos comunicacion
  start();
  //medir temperatura 00000011=0x03, medida de temperatura 14 bits)
  setInstruccion(0x03);
  //Ahora esperaremos a que el sensor diga que ha terminado la operacion
  //en ese momento el sensor pondra data a 0
  //CUIDADO SI NO ESTAN BIEN LOS TIEMPOS EL PODRA COGER EL ACK ANTERIOR
  while(ack==HIGH){
    ack=digitalRead(shtData);
  }
  //Justo aqui es cuando termina la medicion y debemos adquirir los datos de tempreatura
  tomarMedicion(0x03);
  omitirCRC();
  
  
  /** --------------------------------------------------------------------------------------------------------
  OBTENER HUMEDAD
  --------------------------------------------------------------------------------------------------------*/
  start();
  //(por ejemplo medir temeperatura 00000101=0x05, medida de humedad 12 bits)
  setInstruccion(0x05);
  //Ahora esperaremos a que el sensor diga que ha terminado la operacion
  //en ese momento el sensor pondra data a 0
  //CUIDADO SI NO ESTAN BIEN LOS TIEMPOS EL PODRA COGER EL ACK ANTERIOR
  while(ack==HIGH){
    ack=digitalRead(shtData);
  }
  tomarMedicion(0x05);
  omitirCRC();
  //Una vez obtenidos los datos pasamos a calcular la temperatura y humedad con sus correciones
  calcularTH();
  delay(4000); //para que no se caliente el sensor no debe estar activo mas de un 10% del tiempo
                //se debe ajustar a 4000 ya que el sensor esta activo 320(14bits)+80(12bits)
   
  datosTemp=0;
  datosHumedad=0;
 
} 
void calcularTH(){
  Serial.print("temperatura :");
  datosTempCorregido=d1+d2*datosTemp; //No entiendo que es el dewPOINT
  Serial.print(datosTempCorregido,4);
  Serial.println("C"); 
  Serial.print("Humedad :");
  datosHumedadCorregido=c1+c2*datosHumedad+c3*pow(datosHumedad,2); //Habria que usar la correción segun la temperatura
  datosHumedadCorregido2=(datosTempCorregido-25)*(t1+t2*datosHumedad)+datosHumedadCorregido;
  Serial.print(datosHumedadCorregido2,4);  
  Serial.println("%");

}
void setInstruccion(byte comando){
  byte valor;
  for (int i=7; i>=0; i--){
    valor=bitRead(comando,i);
    if (valor==1){
      digitalWrite(shtData,HIGH);
    }
    else{
      digitalWrite(shtData,LOW);
    }
    digitalWrite(shtCLK,HIGH);
    delay(10);
    digitalWrite(shtCLK,LOW);
    delay(10);
  }
  //Recibimos ack del sensor
  digitalWrite(shtCLK,HIGH);
  pinMode(shtData, INPUT);
  delay(10);
  ack=digitalRead(shtData);
  if(ack!=LOW){
    //ack=digitalRead(shtData);
    Serial.println("Error de ACK al comando lectura de temperatura");
  }
  digitalWrite(shtCLK,LOW);
  
  ack=digitalRead(shtData);
  if (ack!=HIGH){
    //ack=digitalRead(shtData);
    Serial.println("ACK errror en el fin de ack");
  }
  
}

void tomarMedicion(byte comando){
  int aux=0;
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    if(comando==0x03){
    datosTemp=datosTemp*2+digitalRead(shtData);
    }
    if(comando==0x05){
      datosHumedad=datosHumedad*2+digitalRead(shtData);
    }
    digitalWrite(shtCLK,LOW);
  }
  
  if(comando==0x03){
    datosTemp *=256;
    }
  if(comando==0x05){
    datosHumedad*=256;
  }
  
  //AQUI DEBEMOS ENVIAR ACK ASINTIENDO EL PRIMER BYTE
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);
  digitalWrite(shtData,LOW);
  digitalWrite(shtCLK,HIGH);
  digitalWrite(shtCLK,LOW);
  pinMode(shtData,INPUT);
  
  for (int i=0;i<8;++i){
    digitalWrite(shtCLK,HIGH);
    delay(10);
    aux=aux*2+digitalRead(shtData);
    digitalWrite(shtCLK,LOW);
  }
  if(comando==0x03){
    datosTemp=datosTemp+aux;
    }
  if(comando==0x05){
    datosHumedad=datosHumedad+aux;
  }
}

void omitirCRC(){
  //OMITIMOS EL CRC
  pinMode(shtData,OUTPUT);
  digitalWrite(shtData,HIGH);//OMITIMOS EL CRC
  digitalWrite(shtCLK,HIGH);
  delay(10);
  digitalWrite(shtCLK,LOW);
  delay(10);
  delay(50);

}

void start(){
  //Primero empezamos a comunicar con el sensor, bajar shtData mientras 
  //shtCLK esta alto,pulso bajo de SCK una subida de data mientras SCK se mantiene alto
  pinMode(shtData,OUTPUT);
  pinMode(shtCLK,OUTPUT);
  //Serial.print(digitalRead(x));
  digitalWrite(shtData, HIGH);
  //Serial.print(digitalRead(x));
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, LOW);
  //Serial.print(digitalRead(x));
  delay(5);
  digitalWrite(shtCLK, LOW);
  //Serial.println(shtData);
  delay(2);
  digitalWrite(shtCLK,HIGH);
  delay(5);
  digitalWrite(shtData, HIGH);
  //Serial.print(digitalRead(x));
  //Serial.println(shtData);
  delay(5);
  digitalWrite(shtCLK,LOW);
  delay(5);


}
