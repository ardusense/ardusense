//Version 1.0
//DTE-Universidad Rey Juan Carlos David Rodríguez Lorenzo 


#include <Arduino.h>
#include <SD.h> 

//Pin connection and variable declaration for SD card.
File dataFile;
const int chipSelect = 4; 
unsigned long offset=0;

int SAN_ENDPOINT_ID;
int tFlag;
int temp;
int hum;
int accX;
int accY;
int lightLevel;

void setup(){
  Serial.begin(9600);
  Serial.print("Initializting SD card...");//Prepare a SD.
  pinMode(chipSelect, OUTPUT);
  if (!SD.begin(chipSelect)) {
    Serial.println("Initialization failure");
    
  }else{
    Serial.println("Initialization complete.");
  }
}
void loop(){
  
  //Store data in SD card
  
  dataFile=SD.open("data.txt", FILE_WRITE);
  offset=dataFile.position();
  if (dataFile){
    dataFile.print("#");
    dataFile.print(SAN_ENDPOINT_ID);
    dataFile.print(";");
    dataFile.print(tFlag,DEC);
    dataFile.print(";"); // poner , como separador , .CSV
    dataFile.print(temp,DEC);
    dataFile.print("ºC;");
    dataFile.print(hum,DEC);
    dataFile.print("%;");
    dataFile.print(accX,DEC);
    dataFile.print("x;");
    dataFile.print(accY,DEC);
    dataFile.print("y;");
    dataFile.print(lightLevel,DEC);
    dataFile.println("l#");
    dataFile.close();
    
   }else{
     Serial.println("Open operation ERROR in: data.txt");
   }

}




